# README #

Used to create display ads for clients. 

### What is this repository for? ###

* A starting point for creating display ads.

### How do I get set up? ###

* Clone the Master Branch into Source Tree (or your GIT Client)
* Branch off the Master Branch for Each Client
* Branch off the Client branch for each set of ads

### Basic Ad Guidelines ###

* Total file size of each ad with images and css needs to be under 150kb
* Animations should be 100% CSS based
* JavaScript is not allowed by most Ad Companies, other than their proprietary JavaScript 
* Most ads require a 1px black border wrapping the entire Ad
* Run images through https://tinypng.com/ or https://tinyjpg.com/
** Save images from Photoshop at 100% quality before running through the first time on Tiny JPG or Tiny PNG
** If file size is unacceptable, save at a lower quality, then run through Tiny JPG or Tiny PNG again
* Fonts
** Most fonts are too large to import, so if you must use a certain font it should be an image
** If you do want to try loading a custom font, have a fallback, and reference it by name, but don't load it

### Static Ads ###

* If required to provide a static ad, save the ad out as JPEG within the corresponding ad folder in the "static" directory.
* Confirm with PM/designer on what slide/design to save out for the Static. This is often either the final version of the animation, or a layer in PhotoShop titled "Static" 


### Who do I talk to? ###

* Talk to Dave Dallmer or Matt Hammer if you have any questions about this repository